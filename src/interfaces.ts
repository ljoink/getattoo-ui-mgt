export type TObject = { [key: string]: any };
export type TLanguage = TObject;
export type TDispatch = (obj: IAction) => void;
export type TOption = { value: string | boolean; label: string };
export type TRespHandler = { [key: string]: (...args: any) => void };
export type THandleResp = (
  resp: TObject | null,
  respHandler: TRespHandler,
  ...rest: any
) => void;

export type TDispatchCommon =
  | "siteInfo"
  | "nav"
  | "languageArr"
  | "mgtRoleArr"
  | "dialog"
  | "tmpDialog"
  | "initialled"
  | "frame"
  | "settingClose"
  | "scrollTopButton"
  | "dialogLogin"
  | "dialogMask"
  | "mask"
  | "dialogClose"
  | "delete";
export type TDispatchTable =
  | "newsTable"
  | "homeBannerTable"
  | "itemTable"
  | "adminTable"
  | "siteInfoTable";
export type TDispatchTableSearchForm =
  | "homeBannerTableSearch"
  | "itemTableSearch"
  | "adminTableSearch"
  | "siteInfoTableSearch";
export type TDispatchForm =
  | "loginForm"
  | "homeBannerForm"
  | "itemForm"
  | "itemFileForm"
  | "adminForm"
  | "siteInfoForm";
export type TDispatchType =
  | TDispatchCommon
  | TDispatchTable
  | TDispatchTableSearchForm
  | TDispatchForm;

export const FDispatchTableSearchForm = (
  dispatchType: TDispatchTable
): TDispatchTableSearchForm => {
  return `${dispatchType}Search` as TDispatchTableSearchForm;
};

export type TNav = {
  title: string;
  links?: {
    name: string;
    to?: string;
  }[];
};

export interface IState {
  siteInfo: TObject;
  nav: TNav;
  languageArr: TLanguage[];
  mgtRoleArr?: TObject[];
  initialled: boolean;
  frame: {
    setting: {
      show: boolean;
      expandArr: string[];
      [key: string]: any;
    };
    left: {
      expand: boolean;
      menu: {
        expandedArr: string[];
      };
      [key: string]: any;
    };
  };
  dialog: {
    show: boolean;
    component?: React.FC<any>;
    callBack?: (v?: any) => any;
    [key: string]: any;
  };
  scrollTopButton: {
    initialled: boolean;
    visibility: boolean;
  };
  [key: string]: any;
}

export interface IAction {
  type: TDispatchType;
  payload?: any;
}

export interface IContext {
  state: IState;
  dispatch: TDispatch;
}

export interface IForm {
  dispatchType: TDispatchForm;
  fields: IFormField[];
  access: string;
  respHandler?: TRespHandler;
  nav?: TNav;
  className?: string;
  title?: string;
  submitButtonHtml?: string;
  backButton?: boolean;
  csrfProtected?: boolean;
  isViaDialog?: boolean;
  setExtraState?: () => any;
  replaceId?: string;
  [key: string]: any;
}

export interface IFormField {
  name: string;
  deepPath?: string;
  type?: string;
  schema?: string;
  label?: string;
  placeholder?: string;
  autoFocus?: boolean;
  disabled?: boolean;
  className?: string;
  optionArr?: TOption[];
  default?: string | number;
  notSubmit?: boolean;
  [key: string]: any;
}

export interface ISearchForm {
  dispatchType: TDispatchTable;
  searches: ITableSearch[];
  goTo: (page: number) => void;
  [key: string]: any;
}

export interface IInputField {
  dispatchType: TDispatchForm | TDispatchTableSearchForm;
  fields: IFormField[];
  name: string;
  deepPath: string;
  handleChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  lang?: string;
  label?: string;
  className?: string;
  [key: string]: any;
}
export interface ISelectField {
  dispatchType: TDispatchForm | TDispatchTableSearchForm;
  name: string;
  deepPath: string;
  handleChange?: (opt: TOption) => void;
  label?: string;
  className?: string;
  optionArr?: TOption[];
}

export interface IFileForm {
  dispatchType: TDispatchForm;
  fields: IFormField[];
  access: string;
  csrfProtected?: boolean;
  nav?: TNav;
  className?: string;
  title?: string;
  backButton?: boolean;
  separatedUpload?: TObject;
  [key: string]: any;
}

export interface IFileField {
  dispatchType: TDispatchType;
  fields: IFormField[];
  name: string;
  deepPath: string;
  access?: string;
  label?: string;
  [key: string]: any;
}

export interface ITable {
  dispatchType: TDispatchTable;
  fields: IFormField[];
  access: string;
  nav?: TNav;
  className?: string;
  title?: string;
  uniqueKey?: string;
  setExtraState?: (val?: any) => void;
  [key: string]: any;
}

export interface ITableField {
  name: string;
  label: string;
  replace?: TObject;
  schema?: string;
  className?: string;
}

export interface ITableSearch {
  name: string;
  label: string;
  type?: string;
  schema?: string;
  className?: string;
  optionArr?: TOption[];
  default?: any;
}

export interface IError {
  dispatchType: TDispatchType;
  fieldName: string;
  deepPath: string;
}

export interface ISettingDropDown {
  stateKey: string;
  title: string;
  arr: TObject[];
  onClick: (code: string) => any;
  iconClass?: string;
}
