import React from "react";
import { Store } from "../Store";
import http from "../utils/http";

export default function useMgtRoleArrState(): (
  callback?: (val?: any) => any
) => Promise<void> {
  const { state, dispatch } = React.useContext(Store);

  const mgtRoleArrState = async (callback?: (val?: any) => any) => {
    if (!state.mgtRoleArr || state.mgtRoleArr.length === 0) {
      const { data, status } = await http.get("mgt-role");
      if (status === 200) {
        state.mgtRoleArr = data;
        dispatch({
          type: "mgtRoleArr",
          payload: data,
        });
        if (callback) {
          callback(data);
        }
      }
    }
  };

  return mgtRoleArrState;
}
