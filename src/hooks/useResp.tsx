import React from "react";
import { toast } from "react-toastify";

import { Store } from "../Store";
import { THandleResp } from "../interfaces";
import t from "../utils/translation";

export default function useResp(): THandleResp {
  const { dispatch } = React.useContext(Store);

  const alertErr = (dataMsg: string): void => {
    const msgArr = dataMsg.split("\r\n");
    for (const msg of msgArr) {
      if (msg) {
        toast.error(t(msg));
      }
    }
  };

  const handleResp: THandleResp = (resp, respHandler, ...rest) => {
    if (resp) {
      if (resp.status === 401) {
        if (typeof respHandler["401"] === "function") {
          alertErr(resp.data.Msg);
          respHandler["401"](resp.data, ...rest);
        } else {
          alertErr(resp.data.Msg);

          dispatch({
            type: "dialog",
            payload: {
              show: true,
              type: "loginForm",
            },
          });
        }
      } else {
        if (typeof respHandler[resp.status] === "function") {
          respHandler[resp.status](resp.data, ...rest);
        } else if (typeof respHandler.else === "function") {
          alertErr(resp.data.Msg);
          respHandler.else(resp.data, ...rest);
        } else {
          alertErr(resp.data.Msg);
        }
      }
      if (typeof respHandler.finally === "function") {
        respHandler.finally(resp.data, ...rest);
      }
    } else {
      alertErr("Server Error");
    }
  };

  return handleResp;
}
