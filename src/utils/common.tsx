import { TObject, IFormField, TLanguage } from "../interfaces";
import { serialize } from "object-to-formdata";

export const arrayToObject = (array: any[]): TObject => {
  return Object.assign({}, ...array);
};

export const isDuplicatedArr = (arr: any[], key?: string): boolean => {
  const uniqueArr: string[] = [];

  for (const i in arr) {
    const value = key ? arr[i][key] : arr[i];
    if (uniqueArr.indexOf(value) === -1) {
      uniqueArr.push(value);
    } else {
      return true;
    }
  }
  return false;
};

export const getUniqueArr = (arr: any[], key?: string): string[] => {
  const uniqueArr: string[] = [];

  for (const i in arr) {
    const value = key ? arr[i][key] : arr[i];
    if (uniqueArr.indexOf(value) === -1) {
      uniqueArr.push(value);
    }
  }
  return uniqueArr;
};

export const toggleElement = (arr: string[], element: string): void => {
  let index = arr.indexOf(element);
  if (index === -1) {
    arr.push(element);
    //using arr.filter will make a new copy
  } else {
    while (index !== -1) {
      arr.splice(index, 1);
      index = arr.indexOf(element);
    }
  }
};

export const getEndpoint = (): string => {
  return process.env.REACT_APP_ENDPOINT || "";
};

export const getBaseUrl = (): string => {
  return process.env.PUBLIC_URL === "/" ? "" : process.env.PUBLIC_URL;
};

const storageKey = "management";
export const getStorage = (key: string): any => {
  const storage = localStorage.getItem(key) || "{}";
  return JSON.parse(storage)[storageKey];
};

export const setStorage = (key: string, value: any): void => {
  const storage = localStorage.getItem(key) || "{}";
  const storageObj = JSON.parse(storage);
  storageObj[storageKey] = value || "";

  localStorage.setItem(key, JSON.stringify(storageObj));
};

export const isInt = (value: any): boolean => {
  return !isNaN(value) && parseInt(value) === Number(value);
};

export const getPayload = (
  certainData: TObject,
  fields: IFormField[],
  languageArrS: TLanguage[]
): FormData => {
  const r: TObject = {};
  for (const field of fields) {
    if (field.notSubmit) {
      continue;
    }

    if (field.name.slice(-1) === "_") {
      for (const language of languageArrS) {
        const key = field.name + language.code;
        if (!certainData.hasOwnProperty(key)) {
          continue;
        }

        r[key] = certainData[key];
      }
    } else {
      const key = field.name;
      if (!certainData.hasOwnProperty(key)) {
        continue;
      }

      let value;
      if (field.type === "number") {
        value = Number(certainData[key]);
      } else if (field.type === "file") {
        if (typeof certainData[key] === "string") {
          continue;
        }
        if (certainData[key].picked) {
          value = certainData[key].picked;
        } else if (certainData[key].delete) {
          value = { delete: true };
        }
      } else {
        value = certainData[key];
      }

      r[key] = value;
    }
  }
  return serialize(r);
};

// implement for ie10 or below
export const getDataSet = (e: React.ChangeEvent<HTMLInputElement>): TObject => {
  const obj: TObject = {};
  if (e.currentTarget.dataset) {
    return e.currentTarget.dataset;
  } else {
    var data = e.target.attributes;
    for (var i = 0; i < data.length; i++) {
      var key = data[i].nodeName;
      if (/^data-\w+$/.test(key)) {
        var value = data[i].nodeValue;
        const tmp = key.match(/^data-(\w+)/);
        if (tmp) {
          const keyName = tmp[1];
          obj[keyName] = value;
        }
      }
    }
  }

  return obj;
};
