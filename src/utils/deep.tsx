import { TObject } from "../interfaces";
import { isInt } from "../utils/common";

const getKey = (val: string): string | number => {
  return isInt(val) ? parseInt(val) : val;
};

const get = (path: string, obj: TObject): any | undefined => {
  const paths = path.split(".");
  let r = obj;
  for (let i = 0; i < paths.length; i++) {
    if (!paths[i]) {
      throw new Error(`Path error. (${path})`);
    }
    const key = getKey(paths[i]);
    if ((isInt(key) && r[key] === undefined) || !r.hasOwnProperty(key)) {
      return undefined;
    }
    r = r[key];
  }
  return r;
};

const set = (
  path: string,
  obj: TObject,
  value: any,
  overwrite?: boolean
): void => {
  const paths: string[] = path.split(".");
  let tmp: TObject = obj;
  for (let i = 0; i < paths.length; i++) {
    if (!paths[i]) {
      throw new Error(`Path error. (${path})`);
    }
    const key = getKey(paths[i]);

    const isLast = i === paths.length - 1;
    if (isLast) {
      const isOverwrite = overwrite === undefined ? true : overwrite;
      if (
        isOverwrite ||
        (isInt(key) && tmp[key] === undefined) ||
        !tmp.hasOwnProperty(key)
      ) {
        tmp[key] = value;
      }
    } else if (!tmp[key]) {
      if (isInt(paths[i + 1])) {
        tmp[key] = [];
      } else {
        tmp[key] = {};
      }
    }
    tmp = tmp[key];
  }
};

const deleteElem = (path: string, obj: TObject): TObject => {
  const paths: string[] = path.split(".");

  let tmp: TObject = obj;
  for (let i = 0; i < paths.length; i++) {
    if (!paths[i]) {
      throw new Error(`Path error. (${path})`);
    }
    const key = getKey(paths[i]);

    if (i === paths.length - 1) {
      delete tmp[key];
    } else {
      if (tmp[key] === undefined) {
        break;
      }
      tmp = tmp[key];
    }
  }

  return obj;
};

export default {
  get,
  set,
  deleteElem,
};
