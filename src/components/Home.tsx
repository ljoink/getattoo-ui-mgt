import React from "react";
import { Store } from "../Store";
import t from "../utils/translation";

export default function Home(): JSX.Element {
  const { dispatch } = React.useContext(Store);

  React.useEffect(() => {
    dispatch({
      type: "nav",
      payload: { title: t("Home") },
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return <></>;
}
