import React from "react";
import { toast } from "react-toastify";
import { useHistory } from "react-router-dom";

import { Store } from "../../Store";
import { IForm, IFormField } from "../../interfaces";
import { getBaseUrl, getStorage } from "../../utils/common";
import auth from "../../utils/auth";
import t from "../../utils/translation";

import Form from "../common/Form";
import deep from "../../utils/deep";

export default function Login(): JSX.Element {
  const { state, dispatch } = React.useContext(Store);
  const dispatchType = "loginForm";

  const dialogS = state.dialog || {};
  const isViaDialog: boolean = dialogS.show && dialogS.type === dispatchType;
  const baseUrl = getBaseUrl();
  const history = useHistory();
  if (auth.isLogin() && !isViaDialog) {
    history.push(baseUrl);
  }

  deep.set("loginForm.data", state, {}, false);

  if (isViaDialog && !state[dispatchType].data.email) {
    const loginInfo = getStorage("LoginInfo");
    dispatch({
      type: dispatchType,
      payload: {
        ...state[dispatchType],
        data: { email: loginInfo && loginInfo.email },
      },
    });
  }

  const fields: IFormField[] = [
    {
      name: "email",
      label: "Email address",
      className: "col-sm-12 floating-label",
      schema: "email;required",
    },
    {
      name: "password",
      type: "password",
      label: "Password",
      schema: "required",
      autoFocus: isViaDialog,
      className: "col-sm-12 floating-label",
    },
  ];

  const respHandler = {
    "200": (data?: any): void => {
      if (data) {
        toast(t("Welcome back"));
        auth.saveStorage(data);
        if (isViaDialog) {
          dispatch({ type: "dialogClose" });
          if (typeof dialogS.callBack === "function") {
            dialogS.callBack(dialogS.callBack);
          }
        } else {
          history.replace(baseUrl);
        }
        dispatch({
          type: "delete",
          payload: dispatchType,
        });
      }
    },
    "401": (): void => {
      delete state[dispatchType].data.password;
      dispatch({ type: dispatchType, payload: state[dispatchType] });
    },
    else: (data?: any): void => {
      if (data) {
        toast.error(t(data.Msg));
      }

      delete state[dispatchType].data.password;
      delete state[dispatchType].submitting;
      dispatch({
        type: dispatchType,
        payload: state[dispatchType],
      });
    },
  };

  const formData: IForm = {
    dispatchType,
    fields,
    access: "admin/login",
    respHandler,
    className: "login-form",
    title: "Login to Your Account",
    submitButtonHtml: "Login",
    isViaDialog,
    backButton: false,
  };

  return (
    <section className="mt-5">
      <Form {...formData} />
    </section>
  );
}
