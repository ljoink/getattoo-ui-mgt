import React from "react";
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";

import { Store } from "../../Store";
import { IForm, IFormField, IState } from "../../interfaces";
import { getStorage } from "../../utils/common";
import t from "../../utils/translation";
import Form from "../common/Form";
import useMgtRoleArrState from "../../hooks/useMgtRoleArrState";
import deep from "../../utils/deep";

export default function Profile() {
  const { state, dispatch } = React.useContext(Store);
  const dispatchType = "itemForm";

  const nav: IState["nav"] = {
    title: "Profile - Edit",
    links: [{ name: "Profile" }],
  };

  const mgtRoleArrState = useMgtRoleArrState();

  const history = useHistory();
  const respHandler = {
    "200": (): void => {
      history.goBack();
      dispatch({
        type: "delete",
        payload: dispatchType,
      });
      toast.success(t("Updated successfully"));
    },
  };

  let roleValue = "";
  if (state.mgtRoleArr && deep.get(`${dispatchType}.data.mgt_role_id`, state)) {
    const roleObj = state.mgtRoleArr.filter(
      (current) =>
        current.id === deep.get(`${dispatchType}.data.mgt_role_id`, state)
    );
    roleValue = deep.get(`0.name_${getStorage("Lang")}`, roleObj);
  }

  const passwordPlaceholder = t(
    "Leave blank if you don't want to change password"
  );
  const fields: IFormField[] = [
    {
      name: "email",
      label: "Email address",
      className: "col-sm-6",
      disabled: true,
    },
    {
      name: "mgt_role_id",
      label: "Role",
      value: roleValue,
      className: "col-sm-6",
      disabled: true,
      notSubmit: true,
    },
    {
      name: "name_",
      label: "Name",
      className: "col-sm-4",
    },
    {
      name: "password",
      label: "New password",
      type: "password",
      className: "border-top pt-2 col-sm-6",
      placeholder: passwordPlaceholder,
    },
    {
      name: "confirmed_password",
      label: "Confirmed password",
      className: "col-sm-6",
      type: "password",
      schema: "match[password]",
      placeholder: passwordPlaceholder,
    },
    {
      name: "old_password",
      label: "Old password",
      type: "password",
      className: "border-bottom pb-3 col-sm-6",
      placeholder: passwordPlaceholder,
    },
  ];

  const formData: IForm = {
    dispatchType,
    fields,
    respHandler,
    nav,
    access: "admin",
    csrfProtected: true,
    replaceId: getStorage("LoginInfo").hashId,
    setExtraState: mgtRoleArrState,
  };

  return <Form {...formData} />;
}
