import React from "react";
import { useParams } from "react-router-dom";

import { Store } from "../../Store";
import { IForm, IFormField, TObject, IState, TOption } from "../../interfaces";
import Form from "../common/Form";
import { getStorage } from "../../utils/common";
import http from "../../utils/http";

export default function Admin() {
  const { state, dispatch } = React.useContext(Store);
  const dispatchType = "adminForm";

  state[dispatchType] = state[dispatchType] || {};

  const params = useParams<{ id?: string }>();
  const nav: IState["nav"] = { title: "Admin - " };
  if (params.id) {
    nav.title = nav.title + "Edit";
    nav.links = [{ name: "Admin", to: "/admin" }, { name: "Edit" }];
  } else {
    nav.title = nav.title + "Create";
    nav.links = [{ name: "Admin", to: "/admin" }, { name: "Create" }];
  }

  const loginInfo = getStorage("LoginInfo");
  const setMgtRoleS = async (): Promise<void> => {
    const { data, status } = await http.get("mgt-role/under-own");

    if (status === 200) {
      state[dispatchType].underMgtRoleArr = data;
      dispatch({
        type: dispatchType,
        payload: state[dispatchType],
      });
    }
  };

  const getMgtRoleOptions = (): TOption[] => {
    const r = state[dispatchType].underMgtRoleArr.map((current: TObject) => {
      return {
        value: current.id,
        label: current[`name_${getStorage("Lang")}`],
      };
    });
    return r;
  };

  const emailObj: IFormField = {
    name: "email",
    label: "Email address",
    className: "col-sm-6",
  };
  if (params.id) {
    emailObj.disabled = true;
  } else {
    emailObj.schema = "required;email";
  }

  const fields: IFormField[] = [emailObj];

  if (
    state[dispatchType].underMgtRoleArr &&
    (!params.id || params.id !== loginInfo.hashId)
  ) {
    // create or not editing own
    fields.push({
      name: "mgt_role_id",
      label: "Role",
      type: "select",
      optionArr: getMgtRoleOptions(),
      className: "col-sm-6",
    });
  }

  fields.push({
    name: "name_",
    label: "Name",
    className: "col-sm-4",
  });

  const pwObj: IFormField = {
    name: "password",
    label: "Password",
    type: "password",
    className: "border-top pt-2 col-sm-6",
    schema: params.id ? "" : "required",
  };
  const confirmPwObj: IFormField = {
    name: "confirmed_password",
    label: "Confirmed password",
    className: "col-sm-6",
    type: "password",
    schema: "match[password]",
  };
  const bottomClass = "border-bottom col-sm-6 pb-3";
  if (!params.id) {
    // create
    confirmPwObj.className = bottomClass;
    fields.push(pwObj);
    fields.push(confirmPwObj);
  }

  const formData: IForm = {
    dispatchType,
    className: "admin-form",
    fields,
    nav,
    access: "admin",
    backButton: true,
    setExtraState: setMgtRoleS,
    csrfProtected: true,
  };

  return <Form {...formData} />;
}
