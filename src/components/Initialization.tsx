import React from "react";

import { Store } from "../Store";
import { IAction, TDispatchType } from "../interfaces";
import http from "../utils/http";
import { getStorage, getUniqueArr, setStorage } from "../utils/common";

export default function Initialization(): null {
  const { state, dispatch } = React.useContext(Store);

  React.useEffect(() => {
    const genPromise = (
      path: string,
      type: TDispatchType
    ): Promise<IAction> => {
      return new Promise<IAction>((resolve) => {
        (async () => {
          const resp = await http.get(path);
          if (resp.status === 200) {
            resolve({ type, payload: resp.data });
          }
        })();
      });
    };

    const storedLang = getStorage("Lang");

    const storeLang = () => {
      let langCode = "";
      const langArr = getUniqueArr(state.languageArr, "code");
      if (!storedLang || langArr.indexOf(storedLang) === -1) {
        const defaultLanguage = state.languageArr.filter(
          (current) => current.is_default === "true"
        );
        langCode =
          defaultLanguage.length === 0 ? "en" : defaultLanguage[0].code;
      }

      if (langCode) {
        setStorage("Lang", langCode);
      }
      document.documentElement.lang = getStorage("Lang");
    };

    const updateTitle = (): void => {
      if (state.siteInfo[`name_${storedLang}`]) {
        document.title = state.siteInfo[`name_${storedLang}`];
      }
    };

    (async () => {
      const actions = await Promise.all([
        genPromise("language", "languageArr"),
        genPromise("site-info/1", "siteInfo"),
      ]);

      actions.forEach((current) => {
        state[current.type] = current.payload;
        dispatch(current);
      });
      dispatch({ type: "initialled", payload: true });

      storeLang();
      updateTitle();
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return null;
}
