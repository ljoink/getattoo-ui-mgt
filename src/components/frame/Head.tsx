import React from "react";

import { Store } from "../../Store";
import Setting from "./setting/Container";

export default function HeadNav(): JSX.Element {
  const { state, dispatch } = React.useContext(Store);
  const settingS = state.frame.setting;

  const toggleLeft = () => {
    state.frame.left.expand = !state.frame.left.expand;
    dispatch({
      type: "frame",
      payload: state.frame,
    });
  };

  const toggleSetting = () => {
    settingS.show = !settingS.show;
    dispatch({
      type: "frame",
      payload: state.frame,
    });
  };

  return (
    <nav className="main-header navbar navbar-expand navbar-dark navbar-navy">
      <ul className="navbar-nav">
        <li className="nav-item">
          <span className="nav-link" onClick={toggleLeft}>
            <i className="fa fa-bars" />
          </span>
        </li>
      </ul>

      <ul className="navbar-nav ml-auto">
        <li className="nav-item dropdown">
          <span
            className={`nav-link${settingS.show ? " active" : ""}`}
            onClick={toggleSetting}
          >
            <i className="fa fa-cog" />
          </span>
          {settingS.show && <Setting />}
        </li>
      </ul>
    </nav>
  );
}
