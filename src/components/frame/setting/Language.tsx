import React from "react";

import { Store } from "../../../Store";
import { setStorage, getStorage } from "../../../utils/common";
import Dropdown from "./Dropdown";

export default function Language(): JSX.Element {
  const { state, dispatch } = React.useContext(Store);

  const handleClick = (code: string): void => {
    document.documentElement.lang = code;
    setStorage("Lang", code);
    dispatch({ type: "settingClose" });
  };

  const title = state.languageArr.filter(
    (current) => current.code === getStorage("Lang")
  )[0].label as string;

  const arr = state.languageArr.filter(
    (current) => current.code !== getStorage("Lang")
  );
  return (
    <Dropdown
      stateKey="LanguageArr"
      title={title}
      arr={arr}
      onClick={handleClick}
      iconClass="fa fa-globe"
    />
  );
}
