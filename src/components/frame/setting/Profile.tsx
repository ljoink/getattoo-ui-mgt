import React from "react";
import { useHistory } from "react-router-dom";

import { Store } from "../../../Store";
import { getStorage, getBaseUrl } from "../../../utils/common";
import t from "../../../utils/translation";
import Dropdown from "./Dropdown";

export default function Profile(): JSX.Element {
  const { dispatch } = React.useContext(Store);

  const title = getStorage("LoginInfo").email;
  const arr = [
    { code: "edit", label: t("Edit") },
    { code: "record", label: t("Record") },
  ];

  const history = useHistory();
  const handleClick = (code: string): void => {
    if (code === "edit") {
      const path = `${getBaseUrl()}/profile`;
      if (history.location.pathname !== path) {
        history.push(path);
      }
      dispatch({ type: "settingClose" });
    }
  };

  return (
    <Dropdown
      stateKey="Profile"
      title={title}
      arr={arr}
      onClick={handleClick}
      iconClass="fa fa-user-cog"
    />
  );
}
