import React from "react";
import { ISettingDropDown } from "../../../interfaces";

import { Store } from "../../../Store";
import { toggleElement } from "../../../utils/common";

export default function Dropdown({
  stateKey: stateKeyP,
  title: titleP,
  arr: arrP,
  onClick: onClickP,
  iconClass: iconClassP,
}: ISettingDropDown): JSX.Element {
  const { state, dispatch } = React.useContext(Store);
  const settingS = state.frame.setting;

  const toggleExpandState = (): void => {
    toggleElement(settingS.expandArr, stateKeyP);
    dispatch({
      type: "frame",
      payload: state.frame,
    });
  };

  return (
    <>
      <div
        className={`dropdown-item has-tree ${
          settingS.expandArr.includes(stateKeyP) && "menu-open"
        }`}
        onClick={toggleExpandState}
      >
        <div>
          <i
            className={`${iconClassP ? iconClassP : "fa fa-project-diagram"}`}
          />
          &nbsp;&nbsp;
          {titleP}
          <i className="fa fa-angle-left right" />
        </div>
      </div>

      {settingS.expandArr.includes(stateKeyP) && (
        <ul>
          {arrP.map((current) => (
            <li
              key={current.code}
              onClick={() => {
                onClickP(current.code);
              }}
            >
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <i className="far fa-circle font-sm" />
              &nbsp;
              {current.label}
            </li>
          ))}
        </ul>
      )}
    </>
  );
}
