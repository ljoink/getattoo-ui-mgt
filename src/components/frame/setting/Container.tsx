import React from "react";

import Profile from "./Profile";
import Language from "./Language";
import Logout from "./Logout";

export default function Setting(): JSX.Element {
  return (
    <div className="dropdown-menu dropdown-menu-lg dropdown-menu-right show">
      <Profile />
      <div className="dropdown-divider"></div>

      <Language />
      <div className="dropdown-divider"></div>

      <Logout />
    </div>
  );
}
