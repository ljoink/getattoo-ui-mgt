import React from "react";
import { toast } from "react-toastify";

import { Store } from "../../../Store";
import auth from "../../../utils/auth";
import { getBaseUrl } from "../../../utils/common";
import http from "../../../utils/http";
import t from "../../../utils/translation";

export default function Logout() {
  const { dispatch } = React.useContext(Store);

  const onConfirm = async (): Promise<void> => {
    dispatch({ type: "dialogMask" });

    const config = http.defaultConfig;
    const csrf = await http.getCsrfToken();
    http.handleCsrf(csrf, config);

    const { data, status } = await http.get("admin/logout", config);
    if (status === 200) {
      auth.removeStorage();
      document.location.href = `${getBaseUrl()}/login`;
    } else {
      toast.error(data.Msg);
    }
  };

  const handleClick = () => {
    dispatch({ type: "settingClose" });
    dispatch({
      type: "dialog",
      payload: {
        show: true,
        title: "Do you logout?",
        type: "confirm",
        onConfirm,
      },
    });
  };

  return (
    <div className="dropdown-item" onClick={handleClick}>
      <i className="fa fa-sign-out-alt" />
      &nbsp;&nbsp;
      {t("Logout")}
    </div>
  );
}
