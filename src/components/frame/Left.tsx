import React from "react";
import { Link } from "react-router-dom";

import { Store } from "../../Store";
import Menu from "./Menu";
import { getBaseUrl } from "../../utils/common";

export default function Left() {
  const { state, dispatch } = React.useContext(Store);

  const foldLeftNav = (): void => {
    state.frame.left.expand = false;
    dispatch({
      type: "frame",
      payload: state.frame,
    });
  };

  return (
    <>
      {state.frame.left.expand && (
        <div className="right-mask" onClick={foldLeftNav}></div>
      )}

      <aside className="main-sidebar sidebar-dark-orange elevation-5">
        <Link className="brand-link navbar-gray" to={`${getBaseUrl()}/`}>
          <img
            src={`${getBaseUrl()}/logo192.png`}
            alt="Logo"
            className="brand-image img-circle elevation-3"
            style={{ opacity: 0.8 }}
          ></img>
          <span className="brand-text font-weight-light">{document.title}</span>
        </Link>

        <div className="sidebar">
          <nav className="mt-2">
            <ul className="nav nav-pills nav-sidebar flex-column nav-flat nav-child-indent">
              <Menu />
            </ul>
          </nav>
        </div>
      </aside>
    </>
  );
}
