import React from "react";

import { ITable, ITableField, ITableSearch } from "../../interfaces";
import Table from "../common/table/Container";

import "../../style/table.scss";
 
export default function News() { 
  const dispatchType = "newsTable";

  const fields: ITableField[] = [
    {
      name: "sort",
      label: "Sort",
      schema: "int;greaterEqual[1]",
      className: "sort",
    },
    {
      name: "name_",
      label: "Name_",
    },
    {
      name: "desc_",
      label: "Description_",
    },
  ];
 
  const searches: ITableSearch[] = [
    {
      name: "name",
      label: "Name",
    },
    {
      name: "desc",
      label: "Description",
    },
    {
      name: "visibility",
      label: "Visibility",
      type: "select",
      optionArr: [
        { value: "all", label: "All" },
        { value: "true", label: "Visible" },
        { value: "false", label: "Hidden" },
      ],
      default: "all",
      className: "col-lg-2 col-md-3 col-sm-4",
    },
  ]; 

  const tableData: ITable = {
    dispatchType,
    fields,
    searches,
    canUpload: true,
    nav: {
      title: "News",
      links: [{ name: "News" }],
    },
    access: "news",
  };

  return <Table {...tableData} />;
}
