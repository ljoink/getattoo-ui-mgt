import React from "react";
import { useHistory } from "react-router-dom";
import queryString from "query-string";

import { Store } from "../../Store";
import {
  ITable,
  ITableField,
  ITableSearch,
  TObject,
  TOption,
  TDispatchTable,
  FDispatchTableSearchForm,
} from "../../interfaces";
import Table from "../common/table/Container";

import { getStorage } from "../../utils/common";
import t from "../../utils/translation";

import "../../style/table.scss";
import deep from "../../utils/deep";
import useMgtRoleArrState from "../../hooks/useMgtRoleArrState";

export default function Admin() {
  const { state, dispatch } = React.useContext(Store);
  const dispatchType: TDispatchTable = "adminTable";
  const searchType = FDispatchTableSearchForm(dispatchType);
  state.mgtRoleArr = state.mgtRoleArr || [];

  const mgtRoleArrState = useMgtRoleArrState();

  const history = useHistory();
  const query = queryString.parse(history.location.search);
  const setSearchMgtRoleS = () => {
    for (const opt of getMgtRoleOptions()) {
      if (String(opt.value) === query["mgt-role-id"]) {
        deep.set(`${searchType}.data.mgt-role-id`, state, opt.value);
        dispatch({
          type: searchType,
          payload: state[searchType],
        });
        break;
      }
    }
  };

  const getMgtRoleOptions = (): TOption[] => {
    const r = [];
    state.mgtRoleArr &&
      state.mgtRoleArr.forEach((current: TObject) => {
        r.push({
          value: current.id,
          label: current[`name_${getStorage("Lang")}`],
        });
      });

    r.unshift({ value: "all", label: t("All") });
    return r;
  };

  const fields: ITableField[] = [
    {
      name: "email",
      label: "Email address",
    },
    {
      name: `mgt_role_id`,
      label: `Role`,
      replace: {
        arr: state.mgtRoleArr,
        filterKey: "id",
        key: `name_${getStorage("Lang")}`,
      },
    },
  ];

  const searches: ITableSearch[] = [
    {
      name: "email",
      label: "Email address",
    },
    {
      name: "mgt-role-id",
      label: "Role",
      type: "select",
      optionArr: getMgtRoleOptions(),
      default: "all",
      className: "col-md-3 col-sm-4",
    },
    {
      name: "visibility",
      label: "Visibility",
      type: "select",
      optionArr: [
        { value: "all", label: "All" },
        { value: "true", label: "Visible" },
        { value: "false", label: "Hidden" },
      ],
      default: "all",
      className: "col-lg-2 col-md-3 col-sm-4",
    },
  ];

  const tableData: ITable = {
    dispatchType,
    fields,
    searches,
    nav: {
      title: "Admin",
      links: [{ name: "Admin" }],
    },
    access: "admin",
    uniqueKey: "hash_id",
    setExtraState: () => mgtRoleArrState(setSearchMgtRoleS),
  };

  return <Table {...tableData} />;
}
