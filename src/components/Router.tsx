import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { ToastContainer } from "react-toastify";

import "react-toastify/dist/ReactToastify.css";

import { Store } from "../Store";
import { AuthRoute } from "./common/AuthRoute";
import Dialog from "./common/Dialog";
import ScrollTopButton from "./common/ScrollTopButton";

import LoginForm from "./forms/Login";
import { getBaseUrl } from "../utils/common";

const Home = React.lazy<any>(() => import("./Home"));
const HomeBannerT = React.lazy<any>(() => import("./tables/HomeBanner"));
const HomeBannerF = React.lazy<any>(() => import("./forms/HomeBanner"));
const NewsT = React.lazy<any>(() => import("./tables/News"));
const ItemT = React.lazy<any>(() => import("./tables/Item"));
const ItemF = React.lazy<any>(() => import("./forms/Item"));
const ItemFileF = React.lazy<any>(() => import("./forms/ItemFile"));
const AdminT = React.lazy<any>(() => import("./tables/Admin"));
const AdminF = React.lazy<any>(() => import("./forms/Admin"));
const SiteInfoT = React.lazy<any>(() => import("./tables/SiteInfo"));
const SiteInfoF = React.lazy<any>(() => import("./forms/SiteInfo"));
const ProfileF = React.lazy<any>(() => import("./forms/Profile"));

export default function Router() {
  const { state } = React.useContext(Store);
  const { initialled: initialledS } = state;
  const baseUrl = getBaseUrl();

  if (initialledS) {
    return (
      <>
        <Switch >
          <Route path={`${baseUrl}/login`} component={LoginForm} exact />

          <AuthRoute path={`${baseUrl}/news`} lazy={NewsT} exact/>

          <AuthRoute
            path={`${baseUrl}/home-banner/form/:id`}
            lazy={HomeBannerF}
            exact/>
          <AuthRoute path={`${baseUrl}/home-banner/form`} lazy={HomeBannerF} exact/>
          <AuthRoute path={`${baseUrl}/home-banner`} lazy={HomeBannerT} exact/>

          <AuthRoute path={`${baseUrl}/item-file/:id`} lazy={ItemFileF} exact/>
          <AuthRoute path={`${baseUrl}/item/form/:id`} lazy={ItemF} exact/>
          <AuthRoute path={`${baseUrl}/item/form`} lazy={ItemF} exact/>
          <AuthRoute path={`${baseUrl}/item`} lazy={ItemT} exact/>

          <AuthRoute path={`${baseUrl}/admin/form/:id`} lazy={AdminF} exact/>
          <AuthRoute path={`${baseUrl}/admin/form`} lazy={AdminF} exact/>
          <AuthRoute path={`${baseUrl}/admin`} lazy={AdminT} exact/>

          <AuthRoute path={`${baseUrl}/site-info/form/:id`} lazy={SiteInfoF} exact/>
          <AuthRoute path={`${baseUrl}/site-info`} lazy={SiteInfoT} exact/>

          <AuthRoute path={`${baseUrl}/profile`} lazy={ProfileF} exact/>

          <AuthRoute path={`${baseUrl}`} lazy={Home} exact/>

          <Redirect to={`${baseUrl}`} exact />
        </Switch>

        <ToastContainer style={{ zIndex: 99999 }} />
        <ScrollTopButton />
        <Dialog />
      </>
    );
  } else {
    return <h3>Loading...</h3>;
  }
}
