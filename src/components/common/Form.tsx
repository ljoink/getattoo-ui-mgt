import React from "react";
import { useParams, useHistory } from "react-router-dom";
import { toast } from "react-toastify";

import { Store } from "../../Store";
import { IForm, IFormField, TObject } from "../../interfaces";
import FieldInput from "./field/Input";
import FieldSelect from "./field/Select";
import FieldFile from "./field/File";

import { isDuplicatedArr, getPayload } from "../../utils/common";
import http from "../../utils/http";
import validation from "../../utils/validation";
import t from "../../utils/translation";
import useResp from "../../hooks/useResp";

import "../../style/form.scss";

export default function Form({
  dispatchType: dispatchTypeP,
  fields: fieldsP,
  access: accessP,
  respHandler: respHandlerP,
  nav: navP,
  backButton: backButtonP,
  setExtraState: setExtraStateP,
  csrfProtected: csrfProtectedP,
  isViaDialog: isViaDialogP,
  replaceId: replaceIdP,
  ...restP
}: IForm): JSX.Element {
  const { state, dispatch } = React.useContext(Store);
  const { languageArr: languageArrS } = state;
  state[dispatchTypeP] = state[dispatchTypeP] || {};

  const params = useParams<{ id?: string }>();
  const id = replaceIdP ? replaceIdP : params.id;

  const history = useHistory();
  React.useEffect(() => {
    if (isDuplicatedArr(fieldsP, "name")) {
      console.log("*** Warning: Field's 'name' duplicated ");
    }

    if (navP) {
      dispatch({
        type: "nav",
        payload: navP,
      });
    }

    initState();

    return (): void => {
      if (id) {
        http.get(`${accessP}/${id}?action=unlock`);
      }
      dispatch({
        type: "delete",
        payload: dispatchTypeP,
      });
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const initState = () => {
    if (id) {
      setState();
    } else {
      setDefaultField();
    }

    if (typeof setExtraStateP === "function") {
      setExtraStateP();
    }
  };

  const updateState = () => {
    dispatch({ type: dispatchTypeP, payload: state[dispatchTypeP] });
  };

  const setDefaultField = () => {
    fieldsP.forEach((field) => {
      if (field.default) {
        if (field.name.slice(-1) === "_") {
          for (const language of languageArrS) {
            state[dispatchTypeP].data[field.name + language.code] =
              field.default;
          }
        } else {
          state[dispatchTypeP].data[field.name] = field.default;
        }
      }
    });
    updateState();
  };

  const handleResp = useResp();
  const setState = async (): Promise<void> => {
    if (!isViaDialogP) {
      dispatch({ type: "dialogMask" });
    }

    const resp = await http.get(`${accessP}/${id}?action=lock`);

    if (!isViaDialogP) {
      dispatch({ type: "dialogClose" });
    }

    const respHandler = {
      "401": () => {
        dispatch({
          type: "dialog",
          payload: {
            show: true,
            type: "loginForm",
            callBack: initState,
          },
        });
      },
      "200": (data: any) => {
        state[dispatchTypeP].data = data;
        updateState();
      },
      else: () => {
        history.goBack();
      },
    };

    handleResp(resp, respHandler);
  };

  const handleSubmit = (e: React.FormEvent): void => {
    e.preventDefault();

    if (!state[dispatchTypeP].submitting) {
      state[dispatchTypeP].submitting = true;

      modifyState();

      if (!isViaDialogP) {
        dispatch({ type: "dialogMask" });
      }

      const errors = validation.validateAll(
        state[dispatchTypeP].data,
        fieldsP,
        languageArrS
      );
      if (Object.keys(errors).length === 0) {
        doSubmit();
      } else {
        //invalid
        state[dispatchTypeP].errors = errors;
        delete state[dispatchTypeP].submitting;
        updateState();

        if (!isViaDialogP) {
          dispatch({ type: "dialogClose" });
        }
      }
    }
  };

  const modifyState = (): void => {
    for (const name in state[dispatchTypeP].data) {
      // Trim value unless 'password'
      if (typeof state[dispatchTypeP].data[name] === "string") {
        let origName = name;
        for (const language of languageArrS) {
          const lang = language.code;
          if (name.indexOf(`_${lang}`) !== -1) {
            origName = name.slice(0, -lang.length);
          }
        }

        const field = fieldsP.filter((field) => field.name === origName);
        if (field[0] && field[0].type !== "password") {
          state[dispatchTypeP].data[name] = state[dispatchTypeP].data[
            name
          ].trim();
        }
      }
    }

    updateState();
  };

  const doSubmit = async (): Promise<void> => {
    const path = `${accessP}${id ? `/${id}` : ""}`;

    const config = http.defaultConfig;
    if (csrfProtectedP) {
      const csrf = await http.getCsrfToken();
      http.handleCsrf(csrf, config);
    }
    if (fieldsP.filter((current) => current.type === "file").length > 0) {
      config.headers["content-type"] = "multipart/form-data";
    }

    const resp = await http.post(
      path,
      getPayload(state[dispatchTypeP].data, fieldsP, languageArrS),
      config
    );

    delete state[dispatchTypeP].submitting;
    updateState();

    if (!isViaDialogP) {
      dispatch({ type: "dialogClose" });
    }

    const respHandler = respHandlerP || {};
    if (!respHandler["200"]) {
      respHandler["200"] = (): void => {
        history.goBack();
        dispatch({
          type: "delete",
          payload: dispatchTypeP,
        });
        const msg = id ? "Updated successfully" : "Created successfully";
        toast.success(t(msg));
      };
    }
    handleResp(resp, respHandler);
  };

  const getFieldData = ({ name, label }: IFormField): TObject[] => {
    let r: TObject[] = [];
    if (name.slice(-1) === "_") {
      //gen different lang field
      r = languageArrS.map((language) => {
        const nameLang = name + language.code;
        const r: TObject = {
          lang: language.code,
          name: nameLang,
          inputID: `${dispatchTypeP}_${nameLang}`,
          isError:
            state[dispatchTypeP].errors &&
            state[dispatchTypeP].errors[nameLang],
        };
        if (label) {
          r.label = `${t(label)}(${language.label})`;
        }

        return r;
      });
    } else {
      r = [
        {
          name,
          inputID: `${dispatchTypeP}_${name}`,
          isError:
            state[dispatchTypeP].errors && state[dispatchTypeP].errors[name],
          label,
        },
      ];
    }
    return r;
  };

  const renderField = ({
    name,
    type,
    label,
    schema,
    className,
    ...fieldRest
  }: IFormField): JSX.Element => {
    fieldRest.placeholder = fieldRest.placeholder
      ? t(fieldRest.placeholder)
      : "";

    const fieldData = getFieldData({ name, label });
    const input = fieldData.map((current) => {
      const id = current.inputID;
      let r;
      if (type === "select") {
        r = (
          <FieldSelect
            key={id}
            dispatchType={dispatchTypeP}
            name={current.name}
            label={current.label}
            className={className}
            optionArr={fieldRest.optionArr}
            deepPath=""
          />
        );
      } else if (type === "file") {
        r = (
          <FieldFile
            key={current.inputID}
            dispatchType={dispatchTypeP}
            fields={fieldsP}
            name={current.name}
            deepPath=""
            access={accessP}
          />
        );
      } else {
        r = (
          <FieldInput
            key={id}
            dispatchType={dispatchTypeP}
            fields={fieldsP}
            name={current.name}
            lang={current.lang}
            type={type}
            label={current.label}
            className={className}
            deepPath=""
            {...fieldRest}
          />
        );
      }

      return r;
    });

    return (
      <div className="row" key={`${dispatchTypeP}_${name}`}>
        {input}
      </div>
    );
  };

  // Call renderField as <RenderField /> will blur after onChange unless renderInput place outside "Form" function
  return (
    <form
      className={restP.className}
      onSubmit={(e) => {
        handleSubmit(e);
      }}
    >
      {backButtonP !== false && (
        <>
          <button
            type="button"
            className="back btn btn-secondary"
            onClick={history.goBack}
          >
            {`< ${t("Back")}`}
          </button>
          <div className="clearfix" />
        </>
      )}
      {restP.title && <p className="title">{t(restP.title)}</p>}

      {fieldsP.map((field: IFormField) => renderField(field))}
      <div className="clearfix" />

      <button
        disabled={state[dispatchTypeP].submitting}
        className="submit btn btn-primary"
      >
        {t(restP.submitButtonHtml || "Submit")}
      </button>
      <div className="clearfix" />
    </form>
  );
}
