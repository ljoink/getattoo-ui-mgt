import React from "react";
import { useParams } from "react-router-dom";
import { toast } from "react-toastify";

import { Store } from "../../../Store";
import { IFileField, TObject } from "../../../interfaces";
import { getPayload, getEndpoint } from "../../../utils/common";
import http from "../../../utils/http";
import deep from "../../../utils/deep";
import validation from "../../../utils/validation";
import t from "../../../utils/translation";
import Error from "./Error";
import Loading from "../Loading";
import useResp from "../../../hooks/useResp";

export default function File({
  dispatchType: dispatchTypeP,
  fields: fieldsP,
  name: nameP,
  label: labelP,
  deepPath: deepPathP,
  separatedUpload: separatedUploadP,
  access: accessP,
}: IFileField): JSX.Element {
  const { state, dispatch } = React.useContext(Store);
  const { languageArr: languageArrS } = state;

  deep.set(`${dispatchTypeP}.data`, state, {}, false);
  const formS = state[dispatchTypeP];

  const dataDeepPath = `data${deepPathP ? `.${deepPathP}` : ""}`;
  const submittingDeepPath = `submitting${deepPathP ? `.${deepPathP}` : ""}`;
  const errDeepPath = `errors${deepPathP ? `.${deepPathP}` : ""}`;

  const certainData = deep.get(dataDeepPath, formS);

  const handlePickFile = (e: React.ChangeEvent<HTMLInputElement>): void => {
    if (e.target.files && e.target.files.length > 0) {
      // certainData[nameP] = certainData[nameP] || {};
      deep.set(nameP, certainData, { picked: e.target.files[0] });
      delete certainData[nameP].delete;
      validation.handleCertain(e, formS, fieldsP, deepPathP);
      dispatch({ type: dispatchTypeP, payload: formS });
    }
  };

  const handelSeparatedUpload = (): void => {
    if (certainData && !deep.get(submittingDeepPath, formS)) {
      deep.set(submittingDeepPath, formS, true);
      dispatch({ type: dispatchTypeP, payload: formS });

      const errors = validation.validateAll(certainData, fieldsP, languageArrS);
      if (Object.keys(errors).length === 0) {
        doSeparatedUpload();
      } else {
        //error
        deep.set(errDeepPath, formS, errors);
        deep.set(submittingDeepPath, formS, false);
        dispatch({
          type: dispatchTypeP,
          payload: formS,
        });
      }
    }
  };

  const handleResp = useResp();
  const params = useParams<{ id?: string }>();
  const doSeparatedUpload = async (): Promise<void> => {
    const config = http.defaultConfig;
    const csrf = await http.getCsrfToken();
    http.handleCsrf(csrf, config);
    config.headers["content-type"] = "multipart/form-data";

    const path = separatedUploadP.path || `${accessP}/${params.id}`;
    const resp = await http.post(
      path,
      getPayload(certainData, fieldsP, languageArrS),
      config
    );

    deep.set(submittingDeepPath, formS, false);
    dispatch({ type: dispatchTypeP, payload: formS });

    const respHandler = {
      "200": (data: TObject): void => {
        toast.success(t("Uploaded successfully"));
        deep.set(dataDeepPath, formS, data);
        dispatch({ type: dispatchTypeP, payload: formS });
      },
    };

    handleResp(resp, separatedUploadP.respHandler || respHandler);
  };

  const handleDelete = (): void => {
    dispatch({
      type: "dialog",
      payload: {
        show: true,
        type: "confirm",
        title: "Do you confirm to delete this file?",
        onConfirm: () => doDelete(),
      },
    });
  };

  const doDelete = async (): Promise<void> => {
    deep.set(nameP, certainData, { delete: true });
    dispatch({ type: dispatchTypeP, payload: formS });

    // deep.set(submittingDeepPath, formS, true);
    // dispatch({ type: dispatchTypeP, payload: formS });

    // const config = http.defaultConfig;
    // const csrf = await http.getCsrfToken();
    // http.handleCsrf(csrf, config);
    // config.headers["content-type"] = "multipart/form-data";

    // const resp = await http.delete(`${accessP}/${params.id}/file`, config);

    // deep.set(submittingDeepPath, formS, false);
    // dispatch({ type: dispatchTypeP, payload: formS });

    // const respHandler = {
    //   "200": (): void => {
    //     toast.success(t("Deleted successfully"));
    //     delete certainData[nameP];
    //     dispatch({ type: dispatchTypeP, payload: formS });
    //   },
    // };

    // handleResp(resp, respHandler);
  };

  const id = `${deepPathP ? `${deepPathP}-` : ""}${nameP}`;
  const isError = deep.get(`${errDeepPath}.${nameP}`, formS);
  const isSubmitting = deep.get(submittingDeepPath, formS);

  let view = <></>;
  if (certainData[nameP] && !certainData[nameP].delete) {
    if (certainData[nameP].picked) {
      if (!isError) {
        view = (
          <img
            className="preview"
            src={URL.createObjectURL(certainData[nameP].picked)}
            alt=""
          />
        );
      }
    } else {
      view = (
        <a
          target="_blank"
          rel="noopener noreferrer"
          href={`${getEndpoint()}${certainData.path}/${certainData[nameP]}`}
        >
          <img
            src={`${getEndpoint()}${certainData.path}/${
              certainData.thumb ? "thumb/" : ""
            }${certainData[nameP]}`}
            alt={certainData[nameP]}
          ></img>
        </a>
      );
    }
  }

  let btnGroup = (
    <>
      <div className="col-sm-12">
        <div className="btn-group">
          <label
            htmlFor={id}
            className={`btn file-label mr-2${isSubmitting ? " disabled" : ""}`}
          >
            <i className="fas fa-file" />
            &nbsp;{t(labelP || "Choose a file")}
          </label>

          {certainData[nameP] && !certainData[nameP].delete && (
            <button
              type="button"
              className="btn btn-danger"
              onClick={handleDelete}
            >
              <i className="fas fa fa-trash-alt" />
              &nbsp;{t(labelP || "Delete file")}
            </button>
          )}
        </div>
      </div>
      <div className="col-sm-12">
        <input
          id={id}
          name={nameP}
          type="file"
          onChange={(e) => handlePickFile(e)}
          className={`file-input${isError ? " is-invalid " : ""}`}
          disabled={isSubmitting}
        />
        {isError && (
          <Error
            dispatchType={dispatchTypeP}
            fieldName={nameP}
            deepPath={deepPathP}
          />
        )}
      </div>
    </>
  );

  return (
    <span key={id}>
      <div className="col-sm-12 mb-2">{view}</div>
      {btnGroup}
    </span>
  );
}
