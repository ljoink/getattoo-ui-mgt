import React from "react";
import RectSelect from "react-select";

import { Store } from "../../../Store";
import { ISelectField, TOption } from "../../../interfaces";
import deep from "../../../utils/deep";
import t from "../../../utils/translation";

export default function Select({
  dispatchType: dispatchTypeP,
  name: nameP,
  label: labelP,
  className: classNameP,
  optionArr: optionArrP,
  deepPath: deepPathP,
  handleChange: handleChangeP,
}: ISelectField): JSX.Element {
  const { state, dispatch } = React.useContext(Store);

  const deepPath = `${dispatchTypeP}.data${deepPathP ? `.${deepPathP}` : ""}`;
  const nameDeepPath = `${deepPath}.${nameP}`;
  deep.set(nameDeepPath, state, {}, false);

  const getSelectValue = (): TOption => {
    return optionArr.filter(
      (current) =>
        String(current.value) === String(deep.get(nameDeepPath, state))
    )[0];
  };

  let handleChange = (_opt: TOption): void => {};
  if (typeof handleChangeP === "function") {
    handleChange = handleChangeP;
  } else {
    handleChange = (opt: TOption): void => {
      deep.set(nameDeepPath, state, opt.value);
      dispatch({ type: dispatchTypeP, payload: state[dispatchTypeP] });
    };
  }

  const id = `${deepPath}-${nameP}`;
  const optionArr = optionArrP || [];
  return (
    <div className={`${classNameP || "col-sm-12"} form-group select`}>
      {labelP && <label htmlFor={id}>{t(labelP)} :</label>}
      <RectSelect
        key={id}
        id={id}
        name={nameP}
        options={optionArr}
        placeholder=" --- "
        value={getSelectValue()}
        onChange={(opt) => handleChange(opt as TOption)}
      />
    </div>
  );
}
