import React from "react";
import { Link, useHistory } from "react-router-dom";
import queryString from "query-string";

import { Store } from "../../../Store";
import {
  TObject,
  ITable,
  ITableSearch,
  TOption,
  FDispatchTableSearchForm,
} from "../../../interfaces";
import validation from "../../../utils/validation";
import { getBaseUrl, isInt } from "../../../utils/common";
import auth from "../../../utils/auth";
import t from "../../../utils/translation";

import http from "../../../utils/http";
import { toast } from "react-toastify";
import TableSearch from "./Search";
import TableHead from "./Head";
import TableBody from "./Body";
import TablePagination from "./Pagination";

export default function Container({
  dispatchType: dispatchTypeP,
  fields: fieldsP,
  searches: searchesP,
  access: accessP,
  nav: navP,
  uniqueKey: uniqueKeyP,
  canUpload: canUploadP,
  setExtraState: setExtraStateP,
}: ITable): JSX.Element {
  const { state, dispatch } = React.useContext(Store);
  const { languageArr: languageArrS } = state;
  state[dispatchTypeP] = state[dispatchTypeP] || {};
  const searchType = FDispatchTableSearchForm(dispatchTypeP);
  state[searchType] = state[searchType] || {};
  const baseUrl = getBaseUrl();
  const history = useHistory();

  React.useEffect(() => {
    if (navP) {
      dispatch({
        type: "nav",
        payload: navP,
      });
    }

    initState();

    return (): void => {
      dispatch({
        type: "delete",
        payload: dispatchTypeP,
      });
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const updateState = () => {
    dispatch({
      type: dispatchTypeP,
      payload: state[dispatchTypeP],
    });
  };

  const updateSearch = () => {
    if (searchesP) {
      dispatch({
        type: searchType,
        payload: state[searchType],
      });
    }
  };

  const initState = () => {
    setState();
    if (typeof setExtraStateP === "function") {
      setExtraStateP();
    }
  };

  const setState = async (): Promise<void> => {
    dispatch({ type: "dialogMask" });

    const { data, status } = await http.get(
      `${accessP}${history.location.search}`
    );

    dispatch({ type: "dialogClose" });

    if (status === 401) {
      popLoginDialog();
    } else if (status === 200) {
      if (data) {
        state[dispatchTypeP].data = data;
      } else {
        if (state[searchType].page !== 1) {
          goTo(1);
          return;
        } else {
          delete state[dispatchTypeP].data;
        }
      }

      const query = queryString.parse(history.location.search);
      if (!validateQuery(query)) {
        history.replace(baseUrl);
      }

      setSearchS(searchesP, query);
      setPageS(query);

      delete state[dispatchTypeP].sort;
      updateState();
    } else {
      toast.error(data.Msg);
    }
  };

  const popLoginDialog = (): void => {
    dispatch({
      type: "dialog",
      payload: {
        show: true,
        type: "loginForm",
        callBack: initState,
      },
    });
  };

  const validateQuery = (query: TObject): boolean => {
    if (Object.keys(query).length !== 0) {
      if (query.page && !isInt(query.page)) {
        return false;
      }
    }
    return true;
  };

  const getQueryStringByState = (): string => {
    const query: TObject = {};

    if (state[searchType].data) {
      for (const key in state[searchType].data) {
        if (state[searchType].data[key]) {
          query[key] = state[searchType].data[key];
        }
      }
    }

    if (state[searchType].page && state[searchType].page !== 1) {
      query.page = state[searchType].page;
    }

    const qyStr = queryString.stringify(query);
    return `${qyStr ? `?${qyStr}` : ""}`;
  };

  const goTo = (page: number): void => {
    state[searchType].page = page;
    updateSearch();

    history.replace(`${baseUrl}/${accessP}${getQueryStringByState()}`);
    initState();
  };

  const setSearchS = (searches: ITableSearch[], query: TObject): void => {
    state[searchType].data = state[searchType].data || {};
    if (searches) {
      searches.forEach((search: ITableSearch) => {
        if (search.type === "select") {
          if (query[search.name] && search.optionArr) {
            for (const opt of search.optionArr) {
              if (String(opt.value) === query[search.name]) {
                state[searchType].data[search.name] = opt.value;
                break;
              }
            }
          } else if (search.default && search.optionArr) {
            if (
              search.optionArr.find(
                (opt: TOption) => search.default === opt.value
              )
            ) {
              state[searchType].data[search.name] = search.default;
            }
          }
        } else {
          if (query[search.name]) {
            state[searchType].data[search.name] = query[search.name];
          }
        }
      });
    }

    state[searchType].lock = true;
  };

  const setPageS = (query: TObject): void => {
    state[searchType].page = query.page || 1;
    updateSearch();
  };

  const handleSubmit = (e: React.FormEvent): void => {
    e.preventDefault();

    if (!state[dispatchTypeP].submitting && state[dispatchTypeP].sort) {
      if (accessP) {
        state[dispatchTypeP].submitting = true;
      }

      updateState();

      const errors = validation.validateArr(
        state[dispatchTypeP].data,
        fieldsP,
        languageArrS
      );
      if (Object.keys(errors).length === 0) {
        doSubmit();
      } else {
        toast.error("Sorting error");
        delete state[dispatchTypeP].submitting;
        updateState();
      }
    }
  };

  const getPayload = (): TObject[] => {
    const payload: TObject[] = [];
    state[dispatchTypeP].sort = state[dispatchTypeP].sort || [];
    for (const id of state[dispatchTypeP].sort) {
      const tmp = state[dispatchTypeP].data.filter(
        (current: TObject) => current.id === id
      )[0];
      if (tmp) {
        payload.push({ id, sort: Number(tmp.sort) });
      }
    }
    return payload;
  };

  const doSubmit = async () => {
    const config = http.defaultConfig;
    const csrf = await http.getCsrfToken();
    http.handleCsrf(csrf, config);

    const { data, status } = await http.post(
      `${accessP}/multi-sort`,
      getPayload(),
      config
    );

    if (status === 401) {
      popLoginDialog();
    } else if (status === 200) {
      toast.success(t("Updated successfully"));
      delete state[dispatchTypeP].sort;
      delete state[dispatchTypeP].submitting;
      updateState();
    } else {
      toast.error(t(data.Msg));
    }
  };

  return (
    <form
      onSubmit={(e) => {
        handleSubmit(e);
      }}
    >
      {searchesP && (
        <>
          <TableSearch
            dispatchType={dispatchTypeP}
            updateState={updateState}
            searches={searchesP}
            goTo={goTo}
          />
          <div className="clearfix" />
          <hr />
        </>
      )}

      {(auth.check(accessP, 1) || auth.check(accessP, 4)) && (
        <>
          <div className="btn-group float-right mb-1">
            {auth.check(accessP, 1) && (
              <Link
                to={`${baseUrl}/${accessP}/form`}
                className="btn btn-primary btn-sm"
              >
                {t("Create")}
              </Link>
            )}

            {auth.check(accessP, 4) &&
              fieldsP.find((field: TObject) => field.name === "sort") && (
                <button
                  type="submit"
                  className="btn btn-warning btn-sm ml-3"
                  disabled={
                    state[dispatchTypeP].submitting ||
                    !state[dispatchTypeP].sort
                  }
                >
                  {t("Update sort")}
                </button>
              )}
          </div>
          <div className="clearfix mb-2" />
        </>
      )}

      <div className="table-container text-nowrap">
        <table className="table table-bordered">
          <TableHead fields={fieldsP} />
          {state[dispatchTypeP].data && (
            <TableBody
              dispatchType={dispatchTypeP}
              fields={fieldsP}
              updateState={updateState}
              access={accessP}
              canUpload={canUploadP}
              baseUrl={baseUrl}
              popLoginDialog={popLoginDialog}
              uniqueKey={uniqueKeyP || "id"}
              initState={initState}
            />
          )}
        </table>
      </div>
      <div className="clearfix mb-3" />

      {state[dispatchTypeP].data && (
        <div className="row justify-content-center">
          <TablePagination
            searchType={searchType}
            dispatchType={dispatchTypeP}
            goTo={goTo}
          />
        </div>
      )}
    </form>
  );
}
